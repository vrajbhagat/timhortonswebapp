package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testDrinksRegular() {
		List <String> types = MealsService.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("The Drinks options are invalid", types != null );
	}
	
	@Test
	public void testDrinksException() {
		List <String> types = MealsService.getAvailableMealTypes(null);
		assertTrue("The Drinks options are invalid" , types.get(0).equals("No Brand Available"));
	}
	
	@Test
	public void testDrinksBoundaryIn() {
		List <String> types = MealsService.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("The Drinks options are invalid", types.size() > 3 );
	}
	
	
	@Test
	public void testDrinksBoundaryOut() {
		List <String> types = MealsService.getAvailableMealTypes(null);
		assertFalse("The Drinks options are invalid" , types.size() < 1);	
	}

}
